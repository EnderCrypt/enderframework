
/**
 * @author endercrypt
 */
module EnderFramework
{
	requires org.apache.logging.log4j;
	requires info.picocli;
	requires org.jgrapht.core;
	requires EnderCommons;
	requires EnderGroundwork;
	requires org.yaml.snakeyaml;
	requires humanize.slim;
	requires org.apache.commons.text;
	
	exports endercrypt.library.framework;
	exports endercrypt.library.framework.utility;
	exports endercrypt.library.framework.utility.common.software;
	exports endercrypt.library.framework.templates.directories;
	exports endercrypt.library.framework.systems.modules;
	exports endercrypt.library.framework.systems.information;
	exports endercrypt.library.framework.systems.exits;
	exports endercrypt.library.framework.systems.configuration;
}
