package endercrypt.library.framework;

@SuppressWarnings("serial")
public class EnderFrameworkException extends Exception
{
	public EnderFrameworkException(String message)
	{
		super(message);
	}
	
	public EnderFrameworkException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
