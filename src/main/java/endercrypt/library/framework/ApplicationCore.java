package endercrypt.library.framework;


import endercrypt.library.framework.systems.argument.ArgumentManager;
import endercrypt.library.framework.systems.configuration.ConfigurationManager;
import endercrypt.library.framework.systems.exits.ExitManager;
import endercrypt.library.framework.systems.information.InformationManager;
import endercrypt.library.framework.systems.modules.ModuleManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ApplicationCore
{
	private static final Logger logger = LogManager.getLogger(ApplicationCore.class);
	
	private final String[] args;
	private final Application application;
	
	private final ExitManager exitManager;
	private final InformationManager informationManager;
	private final ArgumentManager argumentManager;
	private final ModuleManager moduleManager;
	private final ConfigurationManager configurationManager;
	
	protected ApplicationCore(Application application, String[] args) throws EnderFrameworkException
	{
		logger.debug("Starting up ApplicationCore");
		
		this.application = application;
		this.args = args;
		
		this.exitManager = new ExitManager(this);
		this.configurationManager = new ConfigurationManager(this);
		this.informationManager = new InformationManager(this);
		this.argumentManager = new ArgumentManager(this);
		this.moduleManager = new ModuleManager(this);
	}
	
	public String[] getArgs()
	{
		return args;
	}
	
	public Application getApplication()
	{
		return application;
	}
	
	public ExitManager getExitManager()
	{
		return exitManager;
	}
	
	public InformationManager getInformationManager()
	{
		return informationManager;
	}
	
	public ArgumentManager getArgumentManager()
	{
		return argumentManager;
	}
	
	public ModuleManager getModuleManager()
	{
		return moduleManager;
	}
	
	public ConfigurationManager getConfigurationManager()
	{
		return configurationManager;
	}
}
