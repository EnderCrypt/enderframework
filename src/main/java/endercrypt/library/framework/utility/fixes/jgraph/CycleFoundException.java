package endercrypt.library.framework.utility.fixes.jgraph;

public class CycleFoundException extends RuntimeException
{
	private static final long serialVersionUID = -7305238875964351294L;
	
	/**
	 * 
	 */
	
	public CycleFoundException(String message)
	{
		super(message);
	}
	
	public CycleFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
