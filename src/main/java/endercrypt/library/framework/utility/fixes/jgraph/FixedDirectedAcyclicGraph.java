package endercrypt.library.framework.utility.fixes.jgraph;


import endercrypt.library.framework.utility.fixes.FixBrokenException;

import java.util.function.Supplier;

import org.jgrapht.graph.DirectedAcyclicGraph;


public class FixedDirectedAcyclicGraph<V, E> extends DirectedAcyclicGraph<V, E>
{
	private static final long serialVersionUID = 4784668928228717621L;
	
	/**
	 * 
	 */
	
	public FixedDirectedAcyclicGraph(Class<? extends E> edgeClass)
	{
		super(edgeClass);
	}
	
	public FixedDirectedAcyclicGraph(Supplier<V> vertexSupplier, Supplier<E> edgeSupplier, boolean weighted, boolean allowMultipleEdges)
	{
		super(vertexSupplier, edgeSupplier, weighted, allowMultipleEdges);
	}
	
	public FixedDirectedAcyclicGraph(Supplier<V> vertexSupplier, Supplier<E> edgeSupplier, boolean weighted)
	{
		super(vertexSupplier, edgeSupplier, weighted);
	}
	
	public FixedDirectedAcyclicGraph(Supplier<V> vertexSupplier, Supplier<E> edgeSupplier, VisitedStrategyFactory visitedStrategyFactory, TopoOrderMap<V> topoOrderMap, boolean weighted, boolean allowMultipleEdges)
	{
		super(vertexSupplier, edgeSupplier, visitedStrategyFactory, topoOrderMap, weighted, allowMultipleEdges);
	}
	
	public FixedDirectedAcyclicGraph(Supplier<V> vertexSupplier, Supplier<E> edgeSupplier, VisitedStrategyFactory visitedStrategyFactory, TopoOrderMap<V> topoOrderMap, boolean weighted)
	{
		super(vertexSupplier, edgeSupplier, visitedStrategyFactory, topoOrderMap, weighted);
	}
	
	@Override
	public E addEdge(V sourceVertex, V targetVertex)
	{
		try
		{
			return super.addEdge(sourceVertex, targetVertex);
		}
		catch (IllegalArgumentException e)
		{
			handleIllegalArgumentException(e);
			throw new FixBrokenException("unknown IllegalArgumentException exception", e);
		}
	}
	
	private void handleIllegalArgumentException(IllegalArgumentException e)
	{
		String message = e.getMessage();
		if (message == null)
		{
			return;
		}
		if ("Edge would induce a cycle".equals(message))
		{
			throw new CycleFoundException(message);
		}
		if (message.startsWith("no such vertex in graph: "))
		{
			throw e;
		}
		return;
	}
}
