package endercrypt.library.framework.utility.fixes;

public class FixBrokenException extends RuntimeException
{
	private static final long serialVersionUID = 629742014226434125L;
	
	/**
	 * 
	 */
	
	public FixBrokenException(String message)
	{
		super(message);
	}
	
	public FixBrokenException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
