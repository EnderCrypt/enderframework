package endercrypt.library.framework.utility.common.software;

public class SoftwareMissingException extends RuntimeException
{
	private static final long serialVersionUID = -7839696668772614285L;
	
	/**
	 * 
	 */
	
	public SoftwareMissingException()
	{
		super();
	}
	
	public SoftwareMissingException(String message)
	{
		super(message);
	}
	
	public SoftwareMissingException(Throwable cause)
	{
		super(cause);
	}
	
	public SoftwareMissingException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public SoftwareMissingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
