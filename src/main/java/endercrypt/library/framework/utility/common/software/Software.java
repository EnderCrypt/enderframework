package endercrypt.library.framework.utility.common.software;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Software
{
	private static Path[] PATHS = Arrays.stream(System.getenv("PATH").split(":"))
		.map(Paths::get)
		.toArray(Path[]::new);
	
	public static Software resolve(String filename)
	{
		for (Path path : PATHS)
		{
			Path filePath = path.resolve(filename);
			if (isValidSoftware(filePath))
			{
				return resolve(filePath);
			}
		}
		throw new SoftwareMissingException(filename);
	}
	
	private static boolean isValidSoftware(Path path)
	{
		return Files.exists(path) && Files.isRegularFile(path) && Files.isExecutable(path);
	}
	
	public static Software resolve(Path path)
	{
		if (isValidSoftware(path) == false)
		{
			throw new IllegalArgumentException(path + " is not a valid software");
		}
		return new Software(path);
	}
	
	private final Path path;
	
	private Software(Path path)
	{
		this.path = path;
	}
	
	public Path getPath()
	{
		return path;
	}
	
	public ProcessBuilder setup(String... arguments)
	{
		return setup(Arrays.asList(arguments));
	}
	
	public ProcessBuilder setup(List<String> arguments)
	{
		ArrayList<String> commandString = new ArrayList<>();
		commandString.add(path.toString());
		commandString.addAll(arguments);
		
		return new ProcessBuilder(commandString);
	}
}
