package endercrypt.library.framework.utility;


import endercrypt.library.commons.reject.ThrowRejectDueTo;
import endercrypt.library.framework.Application;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class EnderFrameworkUtility
{
	private EnderFrameworkUtility()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static List<Class<?>> getClassHierarchy(Class<?> targetClass)
	{
		Objects.requireNonNull(targetClass, "targetClass");
		
		List<Class<?>> result = new ArrayList<>();
		
		while (targetClass != null)
		{
			result.add(targetClass);
			targetClass = targetClass.getSuperclass();
		}
		
		return result;
	}
	
	public static List<Class<?>> getInterfaceHierarchy(Class<?> targetClass)
	{
		Objects.requireNonNull(targetClass, "targetClass");
		
		List<Class<?>> result = new ArrayList<>();
		
		if (targetClass.isInterface())
		{
			result.add(targetClass);
		}
		
		for (Class<?> superInterface : targetClass.getInterfaces())
		{
			result.addAll(getInterfaceHierarchy(superInterface));
		}
		
		return result;
	}
	
	public static Class<?> getReturnType(Application application, String method, Class<?>... parameters) throws NoSuchMethodException, SecurityException
	{
		Method targetMethod = application.getClass().getMethod(method, parameters);
		return targetMethod.getReturnType();
	}
	
	public static String generateFriendlyClassName(Class<?> targetClass)
	{
		StringBuilder sb = new StringBuilder();
		for (char c : targetClass.getSimpleName().toCharArray())
		{
			if (sb.length() > 0 && c == Character.toUpperCase(c))
			{
				sb.append(" ");
			}
			sb.append(c);
		}
		return sb.toString();
	}
	
	public static boolean isMatchClassArguments(Object[] args, Class<?>[] expected)
	{
		if (args.length != expected.length)
		{
			return false;
		}
		for (int i = 0; i < args.length; i++)
		{
			Object arg = args[i];
			Class<?> expect = expected[i];
			if (expect.isInstance(arg) == false)
			{
				return false;
			}
		}
		return true;
	}
}
