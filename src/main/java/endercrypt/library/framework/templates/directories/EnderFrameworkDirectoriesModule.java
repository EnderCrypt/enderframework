package endercrypt.library.framework.templates.directories;


import endercrypt.library.framework.Application;
import endercrypt.library.framework.systems.modules.CoreModule;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class EnderFrameworkDirectoriesModule<T extends Application> extends CoreModule<T>
{
	private static final Logger logger = LogManager.getLogger(EnderFrameworkDirectoriesModule.class);
	
	private final List<AutoDirectoryPath> directories = new ArrayList<>();
	
	public EnderFrameworkDirectoriesModule(T application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		directories.forEach(AutoDirectoryPath::auto);
	}
	
	public List<AutoDirectoryPath> getDirectories()
	{
		return Collections.unmodifiableList(directories);
	}
	
	protected final Path create(String path)
	{
		Path pathInstance = Paths.get(path);
		AutoDirectoryPath autoDirectory = new AutoDirectoryPath(pathInstance, this::handleException);
		directories.add(autoDirectory);
		return autoDirectory;
	}
	
	protected final Path create(Path parent, String path)
	{
		return create(parent.resolve(path).toString());
	}
	
	protected void handleException(Path path, IOException e)
	{
		logger.fatal("failed to create directory: " + path, e);
		application.exits().ioException();
	}
}
