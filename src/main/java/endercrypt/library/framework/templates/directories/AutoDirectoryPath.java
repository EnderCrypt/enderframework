package endercrypt.library.framework.templates.directories;


import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;


public class AutoDirectoryPath extends DelegatePath
{
	private final ExceptionHandler exceptionHandler;
	
	public AutoDirectoryPath(Path path, ExceptionHandler exceptionHandler)
	{
		super(path);
		this.exceptionHandler = Objects.requireNonNull(exceptionHandler, "exceptionHandler");
	}
	
	public interface ExceptionHandler
	{
		public void onException(Path path, IOException e);
	}
	
	private void onException(String message)
	{
		onException(new IOException(message));
	}
	
	private void onException(IOException e)
	{
		exceptionHandler.onException(getDelegatePath(), e);
	}
	
	protected void auto()
	{
		if (Files.exists(getDelegatePath()))
		{
			if (Files.isDirectory(getDelegatePath()) == false)
			{
				onException(getDelegatePath() + " is not a directory");
			}
			return;
		}
		try
		{
			Files.createDirectories(getDelegatePath());
		}
		catch (IOException e)
		{
			onException(e);
		}
	}
	
	// protect
	
	@Override
	public FileSystem getFileSystem()
	{
		auto();
		return super.getFileSystem();
	}
}
