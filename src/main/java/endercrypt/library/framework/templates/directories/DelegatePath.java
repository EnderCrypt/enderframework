package endercrypt.library.framework.templates.directories;


import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;


public class DelegatePath implements Path
{
	private final Path delegatePath;
	
	public DelegatePath(Path delegatePath)
	{
		this.delegatePath = delegatePath;
	}
	
	public Path getDelegatePath()
	{
		return delegatePath;
	}
	
	@Override
	public void forEach(Consumer<? super Path> action)
	{
		delegatePath.forEach(action);
	}
	
	@Override
	public Spliterator<Path> spliterator()
	{
		return delegatePath.spliterator();
	}
	
	@Override
	public FileSystem getFileSystem()
	{
		return delegatePath.getFileSystem();
	}
	
	@Override
	public boolean isAbsolute()
	{
		return delegatePath.isAbsolute();
	}
	
	@Override
	public Path getRoot()
	{
		return delegatePath.getRoot();
	}
	
	@Override
	public Path getFileName()
	{
		return delegatePath.getFileName();
	}
	
	@Override
	public Path getParent()
	{
		return delegatePath.getParent();
	}
	
	@Override
	public int getNameCount()
	{
		return delegatePath.getNameCount();
	}
	
	@Override
	public Path getName(int index)
	{
		return delegatePath.getName(index);
	}
	
	@Override
	public Path subpath(int beginIndex, int endIndex)
	{
		return delegatePath.subpath(beginIndex, endIndex);
	}
	
	@Override
	public boolean startsWith(Path other)
	{
		return delegatePath.startsWith(other);
	}
	
	@Override
	public boolean startsWith(String other)
	{
		return delegatePath.startsWith(other);
	}
	
	@Override
	public boolean endsWith(Path other)
	{
		return delegatePath.endsWith(other);
	}
	
	@Override
	public boolean endsWith(String other)
	{
		return delegatePath.endsWith(other);
	}
	
	@Override
	public Path normalize()
	{
		return delegatePath.normalize();
	}
	
	@Override
	public Path resolve(Path other)
	{
		return delegatePath.resolve(other);
	}
	
	@Override
	public Path resolve(String other)
	{
		return delegatePath.resolve(other);
	}
	
	@Override
	public Path resolveSibling(Path other)
	{
		return delegatePath.resolveSibling(other);
	}
	
	@Override
	public Path resolveSibling(String other)
	{
		return delegatePath.resolveSibling(other);
	}
	
	@Override
	public Path relativize(Path other)
	{
		return delegatePath.relativize(other);
	}
	
	@Override
	public URI toUri()
	{
		return delegatePath.toUri();
	}
	
	@Override
	public Path toAbsolutePath()
	{
		return delegatePath.toAbsolutePath();
	}
	
	@Override
	public Path toRealPath(LinkOption... options) throws IOException
	{
		return delegatePath.toRealPath(options);
	}
	
	@Override
	public File toFile()
	{
		return delegatePath.toFile();
	}
	
	@Override
	public WatchKey register(WatchService watcher, Kind<?>[] events, Modifier... modifiers) throws IOException
	{
		return delegatePath.register(watcher, events, modifiers);
	}
	
	@Override
	public WatchKey register(WatchService watcher, Kind<?>... events) throws IOException
	{
		return delegatePath.register(watcher, events);
	}
	
	@Override
	public Iterator<Path> iterator()
	{
		return delegatePath.iterator();
	}
	
	@Override
	public int compareTo(Path other)
	{
		return delegatePath.compareTo(other);
	}
	
	@Override
	public boolean equals(Object other)
	{
		return delegatePath.equals(other);
	}
	
	@Override
	public int hashCode()
	{
		return delegatePath.hashCode();
	}
	
	@Override
	public String toString()
	{
		return delegatePath.toString();
	}
}
