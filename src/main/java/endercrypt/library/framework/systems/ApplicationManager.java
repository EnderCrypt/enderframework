package endercrypt.library.framework.systems;


import endercrypt.library.framework.Application;
import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.groundwork.information.Information;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class ApplicationManager
{
	private static final Logger logger = LogManager.getLogger(ApplicationManager.class);
	
	private final ApplicationCore applicationCore;
	
	public ApplicationManager(ApplicationCore applicationCore)
	{
		logger.debug("Setting up " + getClass().getSimpleName() + " ...");
		this.applicationCore = applicationCore;
	}
	
	public final ApplicationCore getApplicationCore()
	{
		return applicationCore;
	}
	
	public final Application getApplication()
	{
		return getApplicationCore().getApplication();
	}
	
	public Information getInformation()
	{
		return getApplicationCore().getInformationManager().getInformation();
	}
}
