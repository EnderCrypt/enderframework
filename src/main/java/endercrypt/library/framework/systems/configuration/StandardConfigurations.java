package endercrypt.library.framework.systems.configuration;


import endercrypt.library.commons.data.DataSource;
import endercrypt.library.groundwork.information.RawInformation;


public interface StandardConfigurations
{
	@Configuration(location = DataSource.RESOURCES, path = "information.yaml")
	public RawInformation getRawInformation();
}
