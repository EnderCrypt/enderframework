package endercrypt.library.framework.systems.configuration;


import endercrypt.library.commons.data.DataSource;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;
import endercrypt.library.framework.systems.ReturnMethodProducerApplicationManager;
import endercrypt.library.framework.utility.EnderFrameworkUtility;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.introspector.BeanAccess;

import humanize.Humanize;


public class ConfigurationManager extends ReturnMethodProducerApplicationManager<StandardConfigurations>
{
	private static final Logger logger = LogManager.getLogger(ConfigurationManager.class);
	
	private Yaml yamlParser = new Yaml();
	private Map<Method, Object> configurationMap = new HashMap<>();
	
	public ConfigurationManager(ApplicationCore applicationCore) throws EnderFrameworkException
	{
		super(applicationCore, "configurations");
		
		yamlParser.setBeanAccess(BeanAccess.FIELD);
		
		List<Class<?>> classHierarchy = EnderFrameworkUtility.getInterfaceHierarchy(getReturnClass());
		Collections.reverse(classHierarchy);
		
		// Statuses
		for (Class<?> targetClass : classHierarchy)
		{
			for (Method method : targetClass.getDeclaredMethods())
			{
				if (method.getParameterCount() != 0)
				{
					throw new EnderFrameworkException("method " + method + " is expected to have zero arguments");
				}
				
				Configuration annotation = method.getAnnotation(Configuration.class);
				if (annotation == null)
				{
					throw new EnderFrameworkException("method " + method + " lacks @Configuration annotation");
				}
				
				DataSource location = annotation.location();
				Path path = Paths.get(annotation.path());
				logger.debug("Reading " + path + " from " + location.toString().toLowerCase());
				byte[] bytes = readConfig(location, path);
				String yaml = new String(bytes, StandardCharsets.UTF_8);
				Object configuration = parseYaml(method, yaml);
				logger.trace("parsed yaml (" + Humanize.binaryPrefix(bytes.length) + ") into: " + configuration);
				configurationMap.put(method, configuration);
			}
		}
	}
	
	private byte[] readConfig(DataSource location, Path path)
	{
		try
		{
			return location.read(Ender.clazz.getCallerClass(4), path).asBytes();
		}
		catch (IOException e)
		{
			logger.fatal("failed to read " + path + " from " + location.toString().toLowerCase(), e);
			getApplicationCore().getExitManager().instance().configurationProblem();
			return null;
		}
	}
	
	private Object parseYaml(Method method, String yaml)
	{
		try
		{
			return yamlParser.loadAs(yaml, method.getReturnType());
		}
		catch (YAMLException e)
		{
			logger.fatal("failed to parse yaml for " + method, e);
			getApplicationCore().getExitManager().instance().configurationProblem();
			return null;
		}
	}
	
	@Override
	protected Object invoke(@SuppressWarnings("unused") Object proxy, Method method, @SuppressWarnings("unused") Object[] args) throws Throwable
	{
		return configurationMap.get(method);
	}
}
