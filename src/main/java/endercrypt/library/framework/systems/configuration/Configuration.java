package endercrypt.library.framework.systems.configuration;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import endercrypt.library.commons.data.DataSource;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Retention(RUNTIME)
@Target(METHOD)
public @interface Configuration
{
	public String path();
	
	public DataSource location() default DataSource.DISK;
}
