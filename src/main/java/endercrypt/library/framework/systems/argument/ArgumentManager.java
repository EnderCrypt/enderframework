package endercrypt.library.framework.systems.argument;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;
import endercrypt.library.framework.systems.ReturnMethodApplicationManager;
import endercrypt.library.groundwork.command.EnderCommandLineParser;
import endercrypt.library.groundwork.command.StandardArguments;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.IHelpSectionRenderer;
import picocli.CommandLine.ParseResult;


public class ArgumentManager extends ReturnMethodApplicationManager<StandardArguments>
{
	private static final Logger logger = LogManager.getLogger(ArgumentManager.class);
	
	private final EnderCommandLineParser<StandardArguments> commandLine;
	private final StandardArguments arguments;
	
	public ArgumentManager(ApplicationCore applicationCore) throws EnderFrameworkException
	{
		super(applicationCore, "arguments");
		arguments = constructArgumentsInstance();
		commandLine = new EnderCommandLineParser<StandardArguments>(getInformation(), arguments);
		
		// exit codes
		commandLine.getHelpSectionMap().put(CommandLine.Model.UsageMessageSpec.SECTION_KEY_EXIT_CODE_LIST_HEADING, new IHelpSectionRenderer()
		{
			@Override
			public String render(picocli.CommandLine.Help help)
			{
				return "Exit Codes:\n";
			}
		});
		commandLine.getHelpSectionMap().put(CommandLine.Model.UsageMessageSpec.SECTION_KEY_EXIT_CODE_LIST, new IHelpSectionRenderer()
		{
			@Override
			public String render(picocli.CommandLine.Help help)
			{
				return getApplicationCore().getExitManager().getStatusList().stream()
					.map(exit -> exit.getCode() + " = " + exit.getDescription())
					.collect(Collectors.joining("\n"));
			}
		});
		
		// parse
		ParseResult result = commandLine.parsePicocli(getApplicationCore().getArgs());
		if (result == null)
		{
			getApplication().exits().badCommandLineArguments();
			return;
		}
		
		// info
		if (getApplicationCore().getApplication().isPrintVersionOnStartup())
		{
			logger.info(getInformation().getName() + " " + getInformation().getVersion());
		}
	}
	
	private StandardArguments constructArgumentsInstance() throws EnderFrameworkException
	{
		try
		{
			Constructor<?> constructor = getReturnClass().getConstructor();
			return (StandardArguments) constructor.newInstance();
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			throw new EnderFrameworkException("failed to construct " + getReturnClass(), e);
		}
	}
	
	@Override
	public StandardArguments instance()
	{
		return arguments;
	}
}
