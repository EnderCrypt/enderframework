package endercrypt.library.framework.systems.exits;

public interface StandardExits
{
	@ExitCode(code = 0, description = "The command executed successfully")
	public void success();
	
	@ExitCode(code = 1, description = "Bad command-line arguments")
	public void badCommandLineArguments();
	
	@ExitCode(code = 2, description = "Initialization Error")
	public void initializationError();
	
	@ExitCode(code = 3, description = "Uncaught Error")
	public void uncaughtException();
	
	@ExitCode(code = 4, description = "I/O exception")
	public void ioException();
	
	@ExitCode(code = 5, description = "Configuration problem")
	public void configurationProblem();
}
