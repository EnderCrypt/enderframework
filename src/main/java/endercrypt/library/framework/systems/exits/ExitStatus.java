package endercrypt.library.framework.systems.exits;


import endercrypt.library.framework.EnderFrameworkException;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ExitStatus implements Comparable<ExitStatus>
{
	private static final Logger logger = LogManager.getLogger(ExitStatus.class);
	
	public static final int MIN_CODE = 0;
	public static final int MAX_CODE = 255;
	
	private final String methodName;
	private final int code;
	private final String description;
	
	protected ExitStatus(String methodName, int code, String description) throws EnderFrameworkException
	{
		this.methodName = methodName;
		this.code = code;
		this.description = description;
		
		if (code < MIN_CODE || code > MAX_CODE)
		{
			throw new EnderFrameworkException("Illegal exit code: " + code);
		}
	}
	
	public String getMethodName()
	{
		return methodName;
	}
	
	public int getCode()
	{
		return code;
	}
	
	public boolean isSuccess()
	{
		return getCode() == 0;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void call(Object[] args)
	{
		String argString = args == null ? "no args" : "args" + Arrays.toString(args);
		logger.trace("Exit called on " + getMethodName() + " (code: " + getCode() + ") with " + argString);
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof ExitStatus)) return false;
		ExitStatus other = (ExitStatus) obj;
		if (code != other.code) return false;
		return true;
	}
	
	@Override
	public int compareTo(ExitStatus other)
	{
		return Integer.compare(getCode(), other.getCode());
	}
}
