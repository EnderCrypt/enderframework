package endercrypt.library.framework.systems.exits;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Retention(RUNTIME)
@Target(METHOD)
public @interface ExitCode
{
	public int code();
	
	public String description() default "untitled";
}
