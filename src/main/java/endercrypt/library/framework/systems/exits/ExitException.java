package endercrypt.library.framework.systems.exits;

@SuppressWarnings("serial")
public class ExitException extends RuntimeException
{
	public ExitException(String message)
	{
		super(message);
	}
	
	public ExitException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
