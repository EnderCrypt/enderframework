package endercrypt.library.framework.systems.exits;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;
import endercrypt.library.framework.systems.ReturnMethodProducerApplicationManager;
import endercrypt.library.framework.systems.modules.ModuleManager;
import endercrypt.library.framework.utility.EnderFrameworkUtility;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ExitManager extends ReturnMethodProducerApplicationManager<StandardExits>
{
	private static final Logger logger = LogManager.getLogger(ExitManager.class);
	
	private final Map<Integer, ExitStatus> statuses = new HashMap<>();
	
	public ExitManager(ApplicationCore applicationCore) throws EnderFrameworkException
	{
		super(applicationCore, "exits");
		
		List<Class<?>> classHierarchy = EnderFrameworkUtility.getInterfaceHierarchy(getReturnClass());
		Collections.reverse(classHierarchy);
		
		// Statuses
		for (Class<?> targetClass : classHierarchy)
		{
			for (Method method : targetClass.getDeclaredMethods())
			{
				absorbExitMethod(method);
			}
		}
	}
	
	private void absorbExitMethod(Method method) throws EnderFrameworkException
	{
		final String friendlyMethodName = method.getDeclaringClass().getSimpleName() + "." + method.getName() + "()";
		
		// parameter is present
		java.lang.reflect.Parameter[] parameters = method.getParameters();
		if (parameters.length != 0)
		{
			throw new EnderFrameworkException(friendlyMethodName + " has unexpected parameters");
		}
		
		// missing ExitCode
		ExitCode exitCode = method.getAnnotation(ExitCode.class);
		if (exitCode == null)
		{
			throw new EnderFrameworkException("Missing @" + ExitCode.class.getSimpleName() + " on " + friendlyMethodName);
		}
		int code = exitCode.code();
		
		// Status already exist
		if (statuses.containsKey(code))
		{
			throw new EnderFrameworkException("cant add " + friendlyMethodName + " as code " + code + " is already in use");
		}
		String description = exitCode.description();
		
		// create
		ExitStatus exitStatus = new ExitStatus(method.getName(), code, description);
		statuses.put(code, exitStatus);
		logger.debug("Registered exit " + code + ": " + description);
	}
	
	@Override
	protected Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		// prepare
		String methodName = method.getName();
		
		ExitStatus exitStatus = statuses.values().stream()
			.filter(exit -> Objects.equals(exit.getMethodName(), methodName))
			.findFirst().orElseThrow(() -> new EnderFrameworkException("missing exit status for " + methodName));
		
		// modules
		ModuleManager moduleManager = getApplicationCore().getModuleManager();
		if (moduleManager != null)
		{
			moduleManager.shutdown();
		}
		
		// info
		exitStatus.call(args);
		
		// verbose
		if (getApplication().isVerboseExit())
		{
			System.out.println("[Exit: " + exitStatus.getCode() + " " + exitStatus.getDescription() + "]");
		}
		
		// exit
		System.exit(exitStatus.getCode());
		
		return null;
	}
	
	public Map<Integer, ExitStatus> getStatuses()
	{
		return Collections.unmodifiableMap(statuses);
	}
	
	public List<ExitStatus> getStatusList()
	{
		return IntStream.range(ExitStatus.MIN_CODE, ExitStatus.MAX_CODE + 1)
			.mapToObj(statuses::get)
			.filter(Objects::nonNull)
			.collect(Collectors.toList());
	}
}
