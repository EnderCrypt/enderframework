package endercrypt.library.framework.systems;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


public abstract class ReturnMethodProducerApplicationManager<T> extends ReturnMethodApplicationManager<T>
{
	private final T instance;
	
	@SuppressWarnings("unchecked")
	public ReturnMethodProducerApplicationManager(ApplicationCore applicationCore, String method, Class<?>... methodParameters) throws EnderFrameworkException
	{
		super(applicationCore, method, methodParameters);
		
		instance = (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { getReturnClass() }, this::invoke);
	}
	
	protected abstract Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
	
	@Override
	public T instance()
	{
		return instance;
	}
}
