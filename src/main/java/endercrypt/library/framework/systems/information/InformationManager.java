package endercrypt.library.framework.systems.information;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.systems.ApplicationManager;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.information.RawInformation;
import endercrypt.library.groundwork.version.NamedVersion;
import endercrypt.library.groundwork.version.Version;


public class InformationManager extends ApplicationManager
{
	private final Information information;
	
	public InformationManager(ApplicationCore applicationCore)
	{
		super(applicationCore);
		RawInformation rawInformation = applicationCore.getConfigurationManager().instance().getRawInformation();
		information = generateInformation(rawInformation);
	}
	
	private Information generateInformation(RawInformation rawInformation)
	{
		String[] versionNames = getApplicationCore().getApplication().versionNames();
		Version version = generateVersion(versionNames, rawInformation.getVersion());
		
		return Information.assemble(rawInformation, version);
	}
	
	private Version generateVersion(String[] names, int[] version)
	{
		if (names == null)
		{
			return Version.fromArray(version);
		}
		else
		{
			return NamedVersion.fromArray(names, version);
		}
	}
	
	@Override
	public Information getInformation()
	{
		return information;
	}
}
