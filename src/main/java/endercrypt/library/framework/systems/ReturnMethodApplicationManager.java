package endercrypt.library.framework.systems;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;
import endercrypt.library.framework.utility.EnderFrameworkUtility;

import java.util.Arrays;


public abstract class ReturnMethodApplicationManager<T> extends ApplicationManager
{
	private final Class<?> method;
	
	public ReturnMethodApplicationManager(ApplicationCore applicationCore, String method, Class<?>... methodParameters) throws EnderFrameworkException
	{
		super(applicationCore);
		String methodString = method + " with arguments " + Arrays.toString(methodParameters);
		try
		{
			this.method = EnderFrameworkUtility.getReturnType(getApplicationCore().getApplication(), method, methodParameters);
		}
		catch (NoSuchMethodException e)
		{
			throw new EnderFrameworkException("could not find method " + methodString, e);
		}
		catch (SecurityException e)
		{
			throw new EnderFrameworkException("permission problem on accessing " + methodString, e);
		}
	}
	
	public final Class<?> getReturnClass()
	{
		return method;
	}
	
	public abstract T instance();
}
