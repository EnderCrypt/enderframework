package endercrypt.library.framework.systems.modules.actor;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.systems.modules.CoreModuleHolder;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import humanize.Humanize;


public abstract class StandardModuleActor extends AbstractModuleActor
{
	private static final Logger logger = LogManager.getLogger(StandardModuleActor.class);
	
	private final String success;
	private final String failed;
	
	public StandardModuleActor(ApplicationCore applicationCore, String success, String failed)
	{
		super(applicationCore);
		this.success = Objects.requireNonNull(success, "success");
		this.failed = Objects.requireNonNull(failed, "failed");
	}
	
	public String getSuccess()
	{
		return success;
	}
	
	public String getFailed()
	{
		return failed;
	}
	
	@Override
	public void onSuccess(CoreModuleHolder module, long time)
	{
		logger.debug("+++ \"" + module.getName() + "\" successfully " + success.toLowerCase() + " (" + Humanize.nanoTime(time) + ")");
	}
	
	@Override
	public void onFail(CoreModuleHolder module, Exception e)
	{
		logger.error("+++ Exception occured while " + failed.toLowerCase() + " " + module.getName(), e);
	}
}
