package endercrypt.library.framework.systems.modules;


import endercrypt.library.framework.Application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class CoreThreadModule<T extends Application> extends CoreModule<T>
{
	private static final Logger logger = LogManager.getLogger(CoreThreadModule.class);
	
	private final Thread thread;
	
	public CoreThreadModule(T application)
	{
		super(application);
		this.thread = new Thread(this::internalThread);
		this.thread.setName(getClass().getSimpleName() + " thread");
	}
	
	public Thread getThread()
	{
		return thread;
	}
	
	@Override
	protected void onStart() throws Exception
	{
		getThread().start();
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		getThread().interrupt();
	}
	
	private void internalThread()
	{
		try
		{
			thread();
		}
		catch (InterruptedException e)
		{
			return;
		}
		catch (Exception e)
		{
			onException(e);
		}
	}
	
	protected abstract void thread() throws Exception;
	
	protected void onException(Exception e)
	{
		logger.fatal(thread.getName() + " crashed", e);
		application.exits().uncaughtException();
	}
}
