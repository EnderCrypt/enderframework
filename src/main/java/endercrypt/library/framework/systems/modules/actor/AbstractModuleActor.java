package endercrypt.library.framework.systems.modules.actor;


import endercrypt.library.framework.Application;
import endercrypt.library.framework.ApplicationCore;

import java.util.Objects;


public abstract class AbstractModuleActor implements ModuleActor
{
	private final ApplicationCore applicationCore;
	
	public AbstractModuleActor(ApplicationCore applicationCore)
	{
		this.applicationCore = Objects.requireNonNull(applicationCore, "application");
	}
	
	public ApplicationCore getApplicationCore()
	{
		return applicationCore;
	}
	
	public Application getApplication()
	{
		return getApplicationCore().getApplication();
	}
	
}
