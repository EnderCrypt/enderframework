package endercrypt.library.framework.systems.modules;


import endercrypt.library.framework.Application;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class CoreModuleDependencies implements Iterable<Class<? extends CoreModule<Application>>>
{
	private Set<Class<? extends CoreModule<Application>>> dependencies = new HashSet<>();
	
	@SuppressWarnings("unchecked")
	public CoreModuleDependencies add(Class<? extends CoreModule<? extends Application>> dependency)
	{
		dependencies.add((Class<? extends CoreModule<Application>>) dependency);
		return this;
	}
	
	@Override
	public Iterator<Class<? extends CoreModule<Application>>> iterator()
	{
		return dependencies.iterator();
	}
}
