package endercrypt.library.framework.systems.modules.exception;

import endercrypt.library.framework.EnderFrameworkException;

@SuppressWarnings("serial")
public class CoreModuleException extends EnderFrameworkException
{
	public CoreModuleException(String message)
	{
		super(message);
	}
	
	public CoreModuleException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
