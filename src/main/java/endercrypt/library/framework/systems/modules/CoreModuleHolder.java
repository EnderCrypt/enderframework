package endercrypt.library.framework.systems.modules;


import endercrypt.library.framework.Application;
import endercrypt.library.framework.systems.modules.exception.CoreModuleException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class CoreModuleHolder
{
	private static final Logger logger = LogManager.getLogger(CoreModuleHolder.class);
	
	@SuppressWarnings("unchecked")
	public static CoreModuleHolder resolve(Application application, Class<? extends CoreModule<Application>> moduleClass) throws CoreModuleException
	{
		try
		{
			Constructor<?> constructor = moduleClass.getConstructor(application.getClass());
			CoreModule<Application> coreModule = (CoreModule<Application>) constructor.newInstance(application);
			logger.trace("created a new instance of " + moduleClass);
			return new CoreModuleHolder(coreModule);
		}
		catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e)
		{
			throw new CoreModuleException("failed to construct CoreModule " + moduleClass.getSimpleName(), e);
		}
	}
	
	private final CoreModule<Application> module;
	private CoreModuleState state = CoreModuleState.OFFLINE;
	private CoreModuleDependencies dependencies;
	
	public CoreModuleHolder(CoreModule<Application> module)
	{
		this.module = Objects.requireNonNull(module, "module");
		this.dependencies = Objects.requireNonNull(module.dependencies(), module.getClass().getSimpleName() + " dependencies");
	}
	
	public String getName()
	{
		return getModule().getModuleName();
	}
	
	public CoreModule<Application> getModule()
	{
		return module;
	}
	
	public CoreModuleDependencies getDependencies()
	{
		return dependencies;
	}
	
	public CoreModuleState getState()
	{
		return state;
	}
	
	private void requireState(String action, CoreModuleState target)
	{
		if (state != target)
		{
			throw new IllegalStateException("cant " + action + " a module thats not in " + target + " state");
		}
	}
	
	public final void start() throws Exception
	{
		requireState("start", CoreModuleState.OFFLINE);
		state = CoreModuleState.ONLINE;
		module.start();
	}
	
	public final void shutdown() throws Exception
	{
		requireState("shutdown", CoreModuleState.ONLINE);
		state = CoreModuleState.DEAD;
		module.shutdown();
	}
}
