package endercrypt.library.framework.systems.modules;


import endercrypt.library.framework.Application;
import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.EnderFrameworkException;
import endercrypt.library.framework.systems.ReturnMethodApplicationManager;
import endercrypt.library.framework.systems.modules.actor.ModuleActor;
import endercrypt.library.framework.systems.modules.actor.StandardModuleActor;
import endercrypt.library.framework.systems.modules.actor.StrictModuleActor;
import endercrypt.library.framework.systems.modules.exception.CoreModuleException;
import endercrypt.library.framework.systems.modules.exception.CoreModuleStateException;
import endercrypt.library.framework.utility.EnderFrameworkUtility;
import endercrypt.library.framework.utility.fixes.jgraph.CycleFoundException;
import endercrypt.library.framework.utility.fixes.jgraph.FixedDirectedAcyclicGraph;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import humanize.Humanize;


public class ModuleManager extends ReturnMethodApplicationManager<StandardModules>
{
	private static final Logger logger = LogManager.getLogger(ModuleManager.class);
	
	private final StandardModules modules;
	
	private final Map<Class<?>, CoreModuleHolder> moduleMap = new HashMap<>();
	private final List<CoreModuleHolder> orderedModules = new ArrayList<>();
	
	public ModuleManager(ApplicationCore applicationCore) throws EnderFrameworkException
	{
		super(applicationCore, "modules");
		
		modules = (StandardModules) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { getReturnClass() }, this::invoke);
		
		logger.debug("Collecting " + getInformation().getName() + " modules ...");
		
		// moduleMap
		for (Class<?> targetClass : EnderFrameworkUtility.getInterfaceHierarchy(modules.getClass()))
		{
			for (Method method : targetClass.getDeclaredMethods())
			{
				if (method.getParameterCount() != 0)
				{
					throw new EnderFrameworkException(method + " " + method.getName() + " has parameters despite none being allowed");
				}
				Class<? extends CoreModule<Application>> moduleClass = extractModuleClass(method);
				CoreModuleHolder holder = CoreModuleHolder.resolve(getApplication(), moduleClass);
				moduleMap.put(holder.getModule().getClass(), holder);
			}
		}
		
		// prepare for priority sort
		Graph<CoreModuleHolder, DefaultEdge> topologicalModulePriorityGraph = new FixedDirectedAcyclicGraph<>(DefaultEdge.class);
		moduleMap.values().forEach(topologicalModulePriorityGraph::addVertex);
		
		// sort by priority
		for (CoreModuleHolder module : moduleMap.values())
		{
			for (Class<? extends CoreModule<Application>> dependencyClass : module.getDependencies())
			{
				CoreModuleHolder dependency = get(dependencyClass);
				try
				{
					topologicalModulePriorityGraph.addEdge(dependency, module);
				}
				catch (CycleFoundException e)
				{
					throw new EnderFrameworkException("module dependency cycle found on: " + module.getModule().getClass().getSimpleName() + " -> " + dependency.getModule().getClass().getSimpleName());
				}
			}
		}
		
		// finalize
		Iterator<CoreModuleHolder> iterator = new TopologicalOrderIterator<>(topologicalModulePriorityGraph);
		iterator.forEachRemaining(orderedModules::add);
		
		// print
		String modulesString = orderedModules.isEmpty()
			? "(None)"
			: orderedModules.stream()
				.map(CoreModuleHolder::getName)
				.collect(Collectors.joining("\n"));
		
		logger.info("Modules:\n" + modulesString);
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private Object invoke(Object proxy, Method method, Object[] args) throws Throwable
	{
		Class<? extends CoreModule<Application>> returnClass = (Class<? extends CoreModule<Application>>) method.getReturnType();
		return fetchModule(returnClass);
	}
	
	public boolean isInState(CoreModuleState state)
	{
		return moduleMap.values()
			.stream()
			.map(CoreModuleHolder::getState)
			.filter(s -> s != state)
			.findAny()
			.isEmpty();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends CoreModule<Application>> T fetchModule(Class<T> moduleClass) throws CoreModuleException
	{
		CoreModuleHolder holder = get(moduleClass);
		
		if (holder.getState() != CoreModuleState.ONLINE)
		{
			throw new CoreModuleStateException(holder);
		}
		
		return (T) holder.getModule();
	}
	
	@SuppressWarnings("unchecked")
	private static Class<? extends CoreModule<Application>> extractModuleClass(Method method) throws EnderFrameworkException
	{
		Class<?> returnType = method.getReturnType();
		try
		{
			return (Class<? extends CoreModule<Application>>) returnType;
		}
		catch (ClassCastException e)
		{
			throw new EnderFrameworkException(method + " returns non-module class: " + returnType, e);
		}
	}
	
	private <T extends CoreModule<Application>> CoreModuleHolder get(Class<T> moduleClass) throws CoreModuleException
	{
		CoreModuleHolder holder = moduleMap.get(moduleClass);
		if (holder == null)
		{
			throw new CoreModuleException("no module " + moduleClass.getSimpleName() + " is available");
		}
		return holder;
	}
	
	private void performAct(String action, CoreModuleState targetState, boolean forwards, ModuleActor actor)
	{
		action = WordUtils.capitalize(action);
		
		long time = System.nanoTime();
		logger.info(" --{ Performing action \"" + action + "\" }-- ");
		synchronized (modules)
		{
			List<CoreModuleHolder> suitableModules = orderedModules.stream()
				.filter(m -> m.getState() == targetState)
				.collect(Collectors.toList());
			
			if (forwards == false)
			{
				Collections.reverse(suitableModules);
			}
			
			for (CoreModuleHolder moduleHolder : suitableModules)
			{
				try
				{
					long moduleTime = System.nanoTime();
					actor.act(moduleHolder);
					moduleTime = (System.nanoTime() - moduleTime);
					actor.onSuccess(moduleHolder, moduleTime);
				}
				catch (CoreModuleStateException e)
				{
					logger.fatal("Failed to " + action + " " + moduleHolder.getName() + " due to missing module dependancy", e);
					getApplicationCore().getExitManager().instance().initializationError();
				}
				catch (Exception e)
				{
					actor.onFail(moduleHolder, e);
				}
			}
		}
		
		time = System.nanoTime() - time;
		logger.info(" --{ Finished action \"" + action + "\" (" + Humanize.nanoTime(time) + ") }-- ");
		
	}
	
	public void start()
	{
		performAct("Start", CoreModuleState.OFFLINE, true, new StrictModuleActor(getApplicationCore(), "started", "starting up")
		{
			@Override
			public void act(CoreModuleHolder module) throws Exception
			{
				module.start();
			}
		});
	}
	
	public void shutdown()
	{
		performAct("Shutdown", CoreModuleState.ONLINE, false, new StandardModuleActor(getApplicationCore(), "shutdown", "shutting down")
		{
			@Override
			public void act(CoreModuleHolder module) throws Exception
			{
				module.shutdown();
			}
		});
	}
	
	@Override
	public StandardModules instance()
	{
		return modules;
	}
}
