package endercrypt.library.framework.systems.modules.actor;


import endercrypt.library.framework.ApplicationCore;
import endercrypt.library.framework.systems.modules.CoreModuleHolder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class StrictModuleActor extends StandardModuleActor
{
	private static final Logger logger = LogManager.getLogger(StrictModuleActor.class);
	
	public StrictModuleActor(ApplicationCore applicationCore, String success, String failed)
	{
		super(applicationCore, success, failed);
	}
	
	@Override
	public void onFail(CoreModuleHolder module, Exception e)
	{
		super.onFail(module, e);
		logger.fatal("action \n" + getFailed() + "\n failed on EnderFramework", e);
		getApplicationCore().getExitManager().instance().initializationError();
	}
}
