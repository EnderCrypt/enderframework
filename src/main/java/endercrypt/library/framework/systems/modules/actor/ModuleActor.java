package endercrypt.library.framework.systems.modules.actor;


import endercrypt.library.framework.systems.modules.CoreModuleHolder;


public interface ModuleActor
{
	public void act(CoreModuleHolder module) throws Exception;
	
	public void onSuccess(CoreModuleHolder module, long time);
	
	public void onFail(CoreModuleHolder module, Exception e);
}
