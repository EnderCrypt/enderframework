package endercrypt.library.framework.systems.modules;


import endercrypt.library.commons.misc.Ender;
import endercrypt.library.framework.Application;

import java.util.Objects;


public abstract class CoreModule<T extends Application>
{
	protected final T application;
	
	public CoreModule(T application)
	{
		this.application = Objects.requireNonNull(application, "application");
	}
	
	public String getModuleName()
	{
		return Ender.clazz.getHumanReadableClassName(getClass());
	}
	
	public CoreModuleDependencies dependencies()
	{
		return new CoreModuleDependencies();
	}
	
	protected final void shutdown() throws Exception
	{
		onShutdown();
	}
	
	protected void onShutdown() throws Exception
	{
		// do nothing
	}
	
	protected final void start() throws Exception
	{
		onStart();
	}
	
	protected void onStart() throws Exception
	{
		// do nothing
	}
}
