package endercrypt.library.framework.systems.modules.exception;


import endercrypt.library.framework.systems.modules.CoreModuleHolder;


@SuppressWarnings("serial")
public class CoreModuleStateException extends RuntimeException
{
	private final CoreModuleHolder moduleHolder;
	
	public CoreModuleStateException(CoreModuleHolder moduleHolder)
	{
		super(moduleHolder.getModule().getClass().getSimpleName() + " is not online, but is " + moduleHolder.getState().toString().toLowerCase());
		this.moduleHolder = moduleHolder;
	}
	
	public CoreModuleHolder getModuleHolder()
	{
		return moduleHolder;
	}
}
