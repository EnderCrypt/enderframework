package endercrypt.library.framework.systems.modules;

public enum CoreModuleState
{
	OFFLINE,
	ONLINE,
	DEAD;
}
