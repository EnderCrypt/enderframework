package endercrypt.library.framework;


import endercrypt.library.commons.interfaces.Code;
import endercrypt.library.framework.systems.configuration.StandardConfigurations;
import endercrypt.library.framework.systems.exits.StandardExits;
import endercrypt.library.framework.systems.modules.CoreModuleState;
import endercrypt.library.framework.systems.modules.StandardModules;
import endercrypt.library.groundwork.command.StandardArguments;
import endercrypt.library.groundwork.information.Information;

import java.lang.reflect.Constructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class Application
{
	private static final Logger logger = LogManager.getLogger(Application.class);
	
	public static <T extends Application> T launch(Class<T> applicationClass, String[] args)
	{
		try
		{
			// construct application
			Constructor<T> constructor = applicationClass.getConstructor();
			T application = constructor.newInstance();
			
			// systems
			ApplicationCore system = new ApplicationCore(application, args);
			
			// init application
			application.init(system);
			
			return application;
		}
		catch (EnderFrameworkException e)
		{
			logger.fatal("Failed to setup " + applicationClass.getSimpleName(), e);
		}
		catch (Exception e)
		{
			logger.fatal("Unexpected exception on setting up " + applicationClass.getSimpleName(), e);
		}
		return null;
	}
	
	private ApplicationCore applicationCore;
	
	void init(ApplicationCore applicationCore)
	{
		// arguments
		this.applicationCore = applicationCore;
		
		// main
		protectedLaunch(this::init, "init");
		
		// start modules
		applicationCore.getModuleManager().start();
		
		// main
		protectedLaunch(this::main, "main");
	}
	
	private void protectedLaunch(Code method, String methodName)
	{
		try
		{
			method.run();
		}
		catch (Exception e)
		{
			logger.fatal(getClass().getSimpleName() + "." + methodName + "() crashed", e);
			exits().uncaughtException();
		}
	}
	
	protected void init() throws Exception
	{
		// do nothing
	}
	
	protected void main() throws Exception
	{
		// do nothing
	}
	
	private ApplicationCore requireApplicationCore()
	{
		if (applicationCore == null)
		{
			throw new IllegalStateException(getClass().getSimpleName() + " has not yet been initialized");
		}
		return applicationCore;
	}
	
	public final boolean isOnline()
	{
		return requireApplicationCore().getModuleManager().isInState(CoreModuleState.ONLINE);
	}
	
	public String[] versionNames()
	{
		return null;
	}
	
	public final Information getInformation()
	{
		return requireApplicationCore().getInformationManager().getInformation();
	}
	
	public boolean isVerboseExit()
	{
		return true;
	}
	
	public boolean isPrintVersionOnStartup()
	{
		return false;
	}
	
	public StandardArguments arguments()
	{
		return requireApplicationCore().getArgumentManager().instance();
	}
	
	public StandardExits exits()
	{
		return requireApplicationCore().getExitManager().instance();
	}
	
	public StandardModules modules()
	{
		return requireApplicationCore().getModuleManager().instance();
	}
	
	public StandardConfigurations configurations()
	{
		return requireApplicationCore().getConfigurationManager().instance();
	}
}
